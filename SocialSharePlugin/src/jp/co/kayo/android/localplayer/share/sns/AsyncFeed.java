
package jp.co.kayo.android.localplayer.share.sns;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import com.facebook.android.FacebookError;
import com.facebook.android.AsyncFacebookRunner.RequestListener;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.widget.Toast;

public class AsyncFeed implements RequestListener {
    private Context context;
    private String lastTweetText;
    private Handler handler;

    public AsyncFeed(Context context, String lastTweetText) {
        this.context = context;
        this.lastTweetText = lastTweetText;
        this.handler = new Handler();
    }

    @Override
    public void onComplete(String response, final Object state) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = pref.edit();
        editor.putLong("lastFeed.time", System.currentTimeMillis());
        editor.putString("lastFeed.text", lastTweetText);
        editor.commit();

        makeToast(true);
    }

    @Override
    public void onIOException(IOException e, Object state) {
        makeToast(false);
    }

    @Override
    public void onFileNotFoundException(FileNotFoundException e, Object state) {
        makeToast(false);
    }

    @Override
    public void onMalformedURLException(MalformedURLException e, Object state) {
        makeToast(false);
    }

    @Override
    public void onFacebookError(FacebookError e, Object state) {
        makeToast(false);
    }

    private void makeToast(final boolean b) {
        handler.post(new Runnable() {

            @Override
            public void run() {
                if (b) {
                    Toast.makeText(context,
                            context.getString(R.string.txt_post_ok),
                            Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(context,
                            context.getString(R.string.txt_post_ng),
                            Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}
