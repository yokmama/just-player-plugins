
package jp.co.kayo.android.localplayer.share.sns;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import jp.co.kayo.android.localplayer.secret.Keys;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

public class ConfigActivity extends PreferenceActivity implements OnPreferenceClickListener {
    CheckBoxPreference twitterRegistCheck;
    CheckBoxPreference facebookRegistCheck;
    SharedPreferences pref;

    Facebook facebook = new Facebook(Keys.FACEBOOK_API_ID);
    AsyncFacebookRunner mAsyncRunner = new AsyncFacebookRunner(facebook);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref);

        pref = PreferenceManager.getDefaultSharedPreferences(this);
        twitterRegistCheck = (CheckBoxPreference) findPreference("key.twitter_register");
        facebookRegistCheck = (CheckBoxPreference) findPreference("key.facebook_register");

        twitterRegistCheck.setOnPreferenceClickListener(this);
        facebookRegistCheck.setOnPreferenceClickListener(this);

        String access_token = pref.getString(Facebook.TOKEN, null);
        long expires = pref.getLong(Facebook.EXPIRES, 0);
        if (access_token != null) {
            facebook.setAccessToken(access_token);
        }
        if (expires != 0) {
            facebook.setAccessExpires(expires);
        }

        refreshOAuth();
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        if (preference.getKey().equals("key.twitter_register")) {

            String oauthToken = pref.getString(SystemConsts.KEY_TWITTER_OAUTH_TOKEN, "");
            String oauthTokenSecret = pref.getString(
                    SystemConsts.KEY_TWITTER_OAUTH_TOKEN_SECRET, "");

            if (oauthToken.length() > 0 && oauthTokenSecret.length() > 0) {
                Editor editor = pref.edit();
                editor.putString(SystemConsts.KEY_TWITTER_OAUTH_TOKEN, "");
                editor.putString(SystemConsts.KEY_TWITTER_OAUTH_TOKEN_SECRET, "");
                editor.putBoolean(SystemConsts.KEY_BTN_TWITTE_STATE, false);
                editor.commit();
                refreshOAuth();
            }
            else {
                auth();
            }

        }
        else if (preference.getKey().equals("key.facebook_register")) {
            if (facebook.isSessionValid()) {
                SharedPreferences.Editor editor = pref.edit();
                editor.putString(Facebook.TOKEN, "");
                editor.putLong(Facebook.EXPIRES, 0);
                editor.putBoolean(SystemConsts.KEY_BTN_FACEBOOK_STATE, false);
                editor.commit();
                refreshOAuth();
                mAsyncRunner.logout(this, new RequestListener() {
                    @Override
                    public void onComplete(String response, Object state) {
                    }

                    @Override
                    public void onIOException(IOException e, Object state) {
                    }

                    @Override
                    public void onFileNotFoundException(FileNotFoundException e,
                            Object state) {
                    }

                    @Override
                    public void onMalformedURLException(MalformedURLException e,
                            Object state) {
                    }

                    @Override
                    public void onFacebookError(FacebookError e, Object state) {
                    }
                });
            } else {
                facebook.authorize(this, new String[] {
                        "publish_stream", "offline_access"
                }, new DialogListener() {
                    @Override
                    public void onComplete(Bundle values) {
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString(Facebook.TOKEN, facebook.getAccessToken());
                        editor.putLong(Facebook.EXPIRES, facebook.getAccessExpires());
                        editor.putBoolean(SystemConsts.KEY_BTN_FACEBOOK_STATE, true);
                        editor.commit();
                        refreshOAuth();
                    }

                    @Override
                    public void onFacebookError(FacebookError error) {
                        Logger.d(error.toString());
                    }

                    @Override
                    public void onError(DialogError e) {
                        Logger.d(e.toString());
                    }

                    @Override
                    public void onCancel() {
                    }
                });
            }
        }
        return false;
    }

    private void auth() {
        Intent intent = new Intent(this, AuthTwitterActivity.class);
        startActivityForResult(intent, 100);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            refreshOAuth();
        }
        else {
            facebook.authorizeCallback(requestCode, resultCode, data);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(Facebook.TOKEN, facebook.getAccessToken());
            editor.putLong(Facebook.EXPIRES, facebook.getAccessExpires());
            editor.putBoolean(SystemConsts.KEY_BTN_FACEBOOK_STATE,
                    facebook.getAccessToken() != null);
            editor.commit();
            refreshOAuth();
        }
    }

    private void refreshOAuth() {
        String oauthToken = pref.getString(SystemConsts.KEY_TWITTER_OAUTH_TOKEN, "");
        String oauthTokenSecret = pref.getString(
                SystemConsts.KEY_TWITTER_OAUTH_TOKEN_SECRET, "");

        if (oauthToken.length() > 0 && oauthTokenSecret.length() > 0) {
            twitterRegistCheck.setChecked(true);
            twitterRegistCheck.setSummary(getString(R.string.lb_register_summary_yes));
        } else {
            twitterRegistCheck.setChecked(false);
            twitterRegistCheck.setSummary(getString(R.string.lb_register_summary_no));
        }

        if (facebook.isSessionValid()) {
            facebookRegistCheck.setChecked(true);
            facebookRegistCheck.setSummary(getString(R.string.lb_register_summary_yes));
        } else {
            facebookRegistCheck.setChecked(false);
            facebookRegistCheck.setSummary(getString(R.string.lb_register_summary_no));
        }
    }
}
