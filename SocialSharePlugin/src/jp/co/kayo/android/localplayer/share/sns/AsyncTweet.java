
package jp.co.kayo.android.localplayer.share.sns;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.OAuthAuthorization;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.widget.Toast;

public class AsyncTweet extends AsyncTask<Void, Void, Boolean> {
    private Context context;
    private String oauthToken;
    private String oauthTokenSecret;
    private String text;
    private String lastTweetText;

    public AsyncTweet(Context context, String oauthToken, String oauthTokenSecret, String text,
            String lastTweetText) {
        this.context = context;
        this.oauthToken = oauthToken;
        this.oauthTokenSecret = oauthTokenSecret;
        this.text = text;
        this.lastTweetText = lastTweetText;
    }

    @Override
    protected Boolean doInBackground(Void... params) {

        // 認証済み（直でTweet)
        AccessToken accessToken = new AccessToken(oauthToken,
                oauthTokenSecret);
        Configuration config = new ConfigurationBuilder().build();
        Twitter twitter = new TwitterFactory(config)
                .getInstance(new OAuthAuthorization(config));
        twitter.setOAuthAccessToken(accessToken);

        try {
            StatusUpdate update = new StatusUpdate(text);
            twitter.updateStatus(update);
            return true;
        } catch (TwitterException e) {
            Logger.e("twitter.updateStatus", e);
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        if (result.equals(true)) {
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
            Editor editor = pref.edit();
            editor.putLong("lastTweet.time", System.currentTimeMillis());
            editor.putString("lastTweet.text", lastTweetText);
            editor.commit();

            Toast.makeText(context,
                    context.getString(R.string.txt_post_ok),
                    Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(context,
                    context.getString(R.string.txt_post_ng),
                    Toast.LENGTH_SHORT).show();
        }
    }

}
