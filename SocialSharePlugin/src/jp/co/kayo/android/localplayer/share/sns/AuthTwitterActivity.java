
package jp.co.kayo.android.localplayer.share.sns;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.IOException;

import jp.co.kayo.android.localplayer.secret.Keys;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class AuthTwitterActivity extends Activity implements View.OnClickListener {
    private AuthTwitterActivity me = this;
    private RequestToken mRequestToken;
    private Twitter _twitter = null;

    class GetRequestTokenTask extends AsyncTask<Void, Void, RequestToken> {
        private ProgressDialog mProgressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(me);
            mProgressDialog.show();
        }

        @Override
        protected RequestToken doInBackground(Void... params) {
            try {
                RequestToken requestToken = getRequestToken();
                return requestToken;
            } catch (TwitterException e) {
                Log.e("Debug", e.getMessage(), e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(RequestToken result) {
            super.onPostExecute(result);
            mProgressDialog.dismiss();

            if (result != null) {
                mRequestToken = result;
                String uriString = mRequestToken.getAuthenticationURL();
                Uri url = Uri.parse(uriString);
                Intent intent = new Intent(Intent.ACTION_VIEW, url);
                startActivity(intent);
            }
        }
    }

    class GetAccessTokenTask extends AsyncTask<Void, Void, AccessToken> {
        private ProgressDialog mProgressDialog;
        private String mPin;

        public GetAccessTokenTask(String mPin) {
            super();
            this.mPin = mPin;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(me);
            mProgressDialog.show();
        }

        @Override
        protected AccessToken doInBackground(Void... params) {
            try {
                AccessToken accessToken = getAccessToken(mRequestToken, mPin);
                return accessToken;
            } catch (TwitterException e) {
                Log.e("Debug", e.getMessage(), e);
            } catch (IOException e) {
                Log.e("Debug", e.getMessage(), e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(AccessToken result) {
            super.onPostExecute(result);
            mProgressDialog.dismiss();
            if (result != null) {
                setResult(Activity.RESULT_OK);
                storeAccessToken(result);
                Toast.makeText(AuthTwitterActivity.this, getString(R.string.txt_twitter_authok),
                        Toast.LENGTH_SHORT).show();
                finish();
            }
            else {
                Toast.makeText(AuthTwitterActivity.this, getString(R.string.txt_twitter_authok),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    
    Twitter getTwitter(){
        if(_twitter == null){
            _twitter = new TwitterFactory().getInstance();
            _twitter.setOAuthConsumer(Keys.CONSUMER_KEY, Keys.CONSUMER_SECRET);
        }
        return _twitter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.auth_twitter);

        findViewById(R.id.GetPinButton).setOnClickListener(this);
        findViewById(R.id.AuthenticateButton).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.GetPinButton) {
            GetRequestTokenTask task = new GetRequestTokenTask();
            task.execute();
        } else if (v.getId() == R.id.AuthenticateButton) {
            EditText pinEdit = (EditText) findViewById(R.id.PinEdit);
            String pin = String.valueOf(pinEdit.getText());

            GetAccessTokenTask task = new GetAccessTokenTask(pin);
            task.execute();
        }
    }

    private RequestToken getRequestToken() throws TwitterException {
        RequestToken requestToken = getTwitter().getOAuthRequestToken();
        return requestToken;
    }

    private AccessToken getAccessToken(RequestToken requestToken, String pin)
            throws TwitterException, IOException {
        AccessToken accessToken = null;
        try {
            if (pin.length() > 0) {
                accessToken = getTwitter().getOAuthAccessToken(requestToken, pin);
            } else {
                accessToken = getTwitter().getOAuthAccessToken();
            }
        } catch (TwitterException te) {
            if (401 == te.getStatusCode()) {
                System.out.println("Unable to get the access token.");
            } else {
                te.printStackTrace();
            }
        }

        return accessToken;
    }

    private void storeAccessToken(AccessToken accessToken) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        Editor editor = pref.edit();
        editor.putString(SystemConsts.KEY_TWITTER_OAUTH_TOKEN, accessToken.getToken());
        editor.putString(SystemConsts.KEY_TWITTER_OAUTH_TOKEN_SECRET, accessToken.getTokenSecret());
        editor.putBoolean(SystemConsts.KEY_BTN_TWITTE_STATE, true);
        editor.commit();
    }

}
