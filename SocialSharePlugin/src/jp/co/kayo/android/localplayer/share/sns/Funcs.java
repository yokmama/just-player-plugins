
package jp.co.kayo.android.localplayer.share.sns;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Funcs {
    public static final String KEY_HASHTAG = "key.hashtag";

    public static boolean isNotEmpty(String s) {
        if (s != null && s.length() > 0) {
            return true;
        }
        return false;
    }

    public static String getTweetString(SharedPreferences pref, String title,
            String artist, boolean youtube) throws UnsupportedEncodingException {
        StringBuilder buf = new StringBuilder();
        buf.append(pref.getString(SystemConsts.KEY_HASHTAG, "#nowplaying"))
                .append(" ").append(title).append(" - ").append(artist);
        if (youtube && isNotEmpty(artist) && isNotEmpty(title)) {
            buf.append(" ");
            buf.append("http://www.youtube.com/results?search_query=")
                    .append(URLEncoder.encode(artist, "UTF-8")).append("+")
                    .append(URLEncoder.encode(title, "UTF-8"));
        }

        return buf.toString();
    }

    public static void tweet(Context context, String text, String lastTweetText) {
        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(context);
        String oauthToken = pref.getString(SystemConsts.KEY_TWITTER_OAUTH_TOKEN, "");
        String oauthTokenSecret = pref.getString(
                SystemConsts.KEY_TWITTER_OAUTH_TOKEN_SECRET, "");

        if (oauthToken.length() > 0 && oauthTokenSecret.length() > 0) {

            AsyncTweet tweet = new AsyncTweet(context, oauthToken, oauthTokenSecret, text,
                    lastTweetText);
            tweet.execute();
        } else {

            // 非認証(Intent連携)
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, text);
            try {
                context.startActivity(intent);
                if (context instanceof Activity) {
                    ((Activity) context).finish();
                }
            } catch (Exception e) {
            }
        }
    }

}
