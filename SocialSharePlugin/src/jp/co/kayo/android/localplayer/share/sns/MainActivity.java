
package jp.co.kayo.android.localplayer.share.sns;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import jp.co.kayo.android.localplayer.secret.Keys;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

public class MainActivity extends Activity implements OnClickListener,
        TextWatcher, OnCheckedChangeListener {
    private String YOUTUBE_LINK = "http://t.co/XXXXXXXX";

    String album = null;
    String artist = null;
    String title = null;

    SharedPreferences pref;
    TextView textview;
    EditText editText;
    ToggleButton btnTwitter;
    ToggleButton btnFacebook;
    CheckBox checkAutoTweet;
    Button btntw;
    Button btnYoutube;
    Facebook facebook = new Facebook(Keys.FACEBOOK_API_ID);
    AsyncFacebookRunner mAsyncRunner = new AsyncFacebookRunner(facebook);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_tweet);

        editText = (EditText) findViewById(R.id.txtMsg);
        pref = PreferenceManager.getDefaultSharedPreferences(this);

        textview = (TextView) findViewById(R.id.lbCount);
        btntw = (Button) findViewById(R.id.btnTweet);
        btnYoutube = (Button) findViewById(R.id.btnYoutube);

        btntw.setOnClickListener(this);

        editText.addTextChangedListener(this);

        btnTwitter = (ToggleButton) findViewById(R.id.btnTwitter);
        btnFacebook = (ToggleButton) findViewById(R.id.btnFacebook);
        checkAutoTweet = (CheckBox) findViewById(R.id.checkAutoTweet);

        refreshOAuth();
        btnTwitter.setOnClickListener(this);
        btnFacebook.setOnClickListener(this);
        btnYoutube.setOnClickListener(this);

        boolean isSkipDialog = pref.getBoolean("key.skipDialog", false);
        checkAutoTweet.setChecked(isSkipDialog);
        checkAutoTweet.setOnCheckedChangeListener(this);

        // IntentからMediaIDを取得する
        Intent it = getIntent();
        if (it != null) {
            album = it.getStringExtra("album");
            artist = it.getStringExtra("artist");
            title = it.getStringExtra("title");
        }

        // Tweet Message 設定
        try {
            editText.setText(Funcs.getTweetString(pref, title, artist, pref.getBoolean("key.add_youtubelink", false)));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        if (isSkipDialog && isSomeChecked() && Funcs.isNotEmpty(title) && Funcs.isNotEmpty(artist)) {
            try {
                // twitter
                Funcs.tweet(this,
                        Funcs.getTweetString(pref,
                                title,
                                artist, pref.getBoolean("key.add_youtubelink", false)), title + artist);
                // facebook
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_settings) {
            Intent intent = new Intent(this, ConfigActivity.class);
            startActivityForResult(intent, 10);
        }
        return true;
    }

    public boolean isSomeChecked() {
        return btnTwitter.isChecked() || btnFacebook.isChecked();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnTweet: {
                String msg = replaceYoutubeLink();
                boolean posted = false;
                if (btnTwitter.isChecked()) {
                    Funcs.tweet(this, msg, title + artist);
                    posted = true;
                }
                if (btnFacebook.isChecked()) {
                    Bundle params = new Bundle();
                    params.putString("message", msg);
                    String access_token = pref.getString(Facebook.TOKEN, null);
                    long expires = pref.getLong(Facebook.EXPIRES, 0);
                    if (access_token != null) {
                        facebook.setAccessToken(access_token);
                    }
                    if (expires != 0) {
                        facebook.setAccessExpires(expires);
                    }

                    // メッセージを投稿する
                    mAsyncRunner.request("me/feed", params, "POST", new AsyncFeed(
                            getApplicationContext(), title + artist) {
                    },
                            null);
                    posted = true;
                }
                if (posted) {
                    finish();
                }
            }
                break;
            case R.id.btnYoutube: {
                addYoutubeLink();
            }
                break;
            case R.id.btnTwitter: {
                boolean isChecked = btnTwitter.isChecked();
                if (isChecked) {
                    // 認証
                    String oauthToken = pref.getString(SystemConsts.KEY_TWITTER_OAUTH_TOKEN, "");
                    String oauthTokenSecret = pref.getString(
                            SystemConsts.KEY_TWITTER_OAUTH_TOKEN_SECRET, "");

                    if (oauthToken.length() > 0 && oauthTokenSecret.length() > 0) {
                        Editor editor = pref.edit();
                        editor.putBoolean(SystemConsts.KEY_BTN_TWITTE_STATE, true);
                        editor.commit();
                    }
                    else {
                        auth();
                    }
                } else {
                    // 認証削除
                    Editor editor = pref.edit();
                    editor.putBoolean("btnTwitter", false);
                    editor.commit();
                    refreshOAuth();
                }

            }
                break;
            case R.id.btnFacebook: {
                boolean isChecked = btnFacebook.isChecked();
                if (isChecked) {
                    String access_token = pref.getString(Facebook.TOKEN, null);
                    long expires = pref.getLong(Facebook.EXPIRES, 0);
                    if (access_token != null) {
                        facebook.setAccessToken(access_token);
                    }
                    if (expires != 0) {
                        facebook.setAccessExpires(expires);
                    }
                    if (facebook.isSessionValid()) {
                        Editor editor = pref.edit();
                        editor.putBoolean(SystemConsts.KEY_BTN_FACEBOOK_STATE, true);
                        editor.commit();
                        // refreshOAuth();
                    }
                    else {
                        facebook.authorize(this, new String[] {
                                "publish_stream", "offline_access"
                        }, new DialogListener() {
                            @Override
                            public void onComplete(Bundle values) {
                                SharedPreferences.Editor editor = pref.edit();
                                editor.putString(Facebook.TOKEN, facebook.getAccessToken());
                                editor.putLong(Facebook.EXPIRES, facebook.getAccessExpires());
                                editor.putBoolean(SystemConsts.KEY_BTN_FACEBOOK_STATE, true);
                                editor.commit();
                                refreshOAuth();
                            }

                            @Override
                            public void onFacebookError(FacebookError error) {
                                Logger.d(error.toString());
                            }

                            @Override
                            public void onError(DialogError e) {
                                Logger.d(e.toString());
                            }

                            @Override
                            public void onCancel() {
                            }
                        });
                    }
                }
                else {
                    // 認証削除
                    Editor editor = pref.edit();
                    editor.putBoolean(SystemConsts.KEY_BTN_FACEBOOK_STATE, false);
                    editor.commit();
                    refreshOAuth();
                }

            }
                break;
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        String str = s.toString();
        final int n = 140 - str.length();
        textview.setText(Integer.toString(n));
        if (n >= 0) {
            btntw.setEnabled(true);
        } else {
            btntw.setEnabled(false);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count,
            int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView.getId() == R.id.checkAutoTweet) {
            Editor editor = pref.edit();
            editor.putBoolean("key.skipDialog", isChecked);
            editor.commit();
        }
    }

    private void addYoutubeLink() {
        StringBuilder buf = new StringBuilder();
        String text = editText.getText().toString();
        buf.append(text);
        buf.append(" ");

        buf.append(YOUTUBE_LINK);

        editText.setText(buf.toString());
    }

    private String replaceYoutubeLink() {
        String text = editText.getText().toString();
        try {
            if (text.indexOf(YOUTUBE_LINK) != -1) {
                StringBuilder url = new StringBuilder();
                url.append("http://www.youtube.com/results?search_query=")
                        .append(URLEncoder.encode(artist, "UTF-8")).append("+")
                        .append(URLEncoder.encode(title, "UTF-8"));

                String youtubelinke = url.toString();
                text = text.replace(YOUTUBE_LINK, youtubelinke);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            text = text.replace(YOUTUBE_LINK, "");
        }
        return text;
    }

    private void auth() {
        Intent intent = new Intent(this, AuthTwitterActivity.class);
        startActivityForResult(intent, 100);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 10) {

            refreshOAuth();
        }
        else if (requestCode == 100) {
            String oauthToken = pref.getString(SystemConsts.KEY_TWITTER_OAUTH_TOKEN, "");
            String oauthTokenSecret = pref.getString(
                    SystemConsts.KEY_TWITTER_OAUTH_TOKEN_SECRET, "");

            if (oauthToken.length() > 0 && oauthTokenSecret.length() > 0) {
                Editor editor = pref.edit();
                editor.putBoolean(SystemConsts.KEY_BTN_TWITTE_STATE, true);
                editor.commit();
            }

            refreshOAuth();
        }
        else {
            facebook.authorizeCallback(requestCode, resultCode, data);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(Facebook.TOKEN, facebook.getAccessToken());
            editor.putLong(Facebook.EXPIRES, facebook.getAccessExpires());
            editor.putBoolean(SystemConsts.KEY_BTN_FACEBOOK_STATE,
                    facebook.getAccessToken() != null);
            editor.commit();
            refreshOAuth();
        }
    }

    private void refreshOAuth() {
        String oauthToken = pref.getString(SystemConsts.KEY_TWITTER_OAUTH_TOKEN, "");
        String oauthTokenSecret = pref.getString(
                SystemConsts.KEY_TWITTER_OAUTH_TOKEN_SECRET, "");

        if (oauthToken.length() > 0 && oauthTokenSecret.length() > 0) {
            btnTwitter.setChecked(pref.getBoolean(SystemConsts.KEY_BTN_TWITTE_STATE, false));
        } else {
            btnTwitter.setChecked(false);
        }

        String access_token = pref.getString(Facebook.TOKEN, null);
        long expires = pref.getLong(Facebook.EXPIRES, 0);
        if (access_token != null && expires != 0) {
            btnFacebook.setChecked(pref.getBoolean(SystemConsts.KEY_BTN_FACEBOOK_STATE, false));
        } else {
            btnFacebook.setChecked(false);
        }
    }
}
