
package jp.co.kayo.android.localplayer.share.sns;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.UnsupportedEncodingException;

import jp.co.kayo.android.localplayer.secret.Keys;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.Facebook;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;

public class AutoTweetService extends Service {
    // 前回のツイートから５秒以上たってないとつぶやけない
    public static final long TWEET_DELAY = 1000 * 5;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            String artist = intent.getStringExtra("artist");
            String title = intent.getStringExtra("title");

            if (Funcs.isNotEmpty(title) && Funcs.isNotEmpty(artist)) {
                SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
                String msg = null;
                try {
                    msg = Funcs.getTweetString(pref,
                            title,
                            artist, pref.getBoolean("key.add_youtubelink", false));
                } catch (UnsupportedEncodingException e) {
                    msg = null;
                    e.printStackTrace();
                }
                if (Funcs.isNotEmpty(msg)) {
                    // Twitter
                    String oauthToken = pref
                            .getString(SystemConsts.KEY_TWITTER_OAUTH_TOKEN, "");
                    String oauthTokenSecret = pref.getString(
                            SystemConsts.KEY_TWITTER_OAUTH_TOKEN_SECRET, "");
                    if (oauthToken.length() > 0 && oauthTokenSecret.length() > 0) {
                        // dupulicate check
                        long lastweetTime = pref.getLong("lastTweet.time", 0);
                        String lasttweetText = pref.getString("lastTweet.text", "");
                        long current = System.currentTimeMillis();
                        if (!lasttweetText.equals(title + artist)
                                && (current - lastweetTime) > TWEET_DELAY) {
                            AsyncTweet tweet = new AsyncTweet(this, oauthToken,
                                    oauthTokenSecret,
                                    msg
                                    , title + artist);
                            tweet.execute();
                        }
                    }

                    // facebook
                    Facebook facebook = new Facebook(Keys.FACEBOOK_API_ID);
                    AsyncFacebookRunner mAsyncRunner = new AsyncFacebookRunner(facebook);
                    Bundle params = new Bundle();
                    params.putString("message", msg);
                    String access_token = pref.getString(Facebook.TOKEN, null);
                    long expires = pref.getLong(Facebook.EXPIRES, 0);
                    if (access_token != null && expires != 0) {
                        facebook.setAccessToken(access_token);
                        facebook.setAccessExpires(expires);
                        if (facebook.isSessionValid()) {
                            // dupulicate check
                            long lastweetTime = pref.getLong("lastFeed.time", 0);
                            String lasttweetText = pref.getString("lastFeed.text", "");
                            long current = System.currentTimeMillis();
                            if (!lasttweetText.equals(title + artist)
                                    && (current - lastweetTime) > TWEET_DELAY) {
                                // メッセージを投稿する
                                mAsyncRunner.request("me/feed", params, "POST", new AsyncFeed(
                                        getApplicationContext(), title + artist) {
                                },
                                        null);
                            }
                        }
                    }
                }
            }
        }

        stopSelf();

        return START_NOT_STICKY;
    }

}
