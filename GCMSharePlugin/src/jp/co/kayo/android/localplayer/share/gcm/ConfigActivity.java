package jp.co.kayo.android.localplayer.share.gcm;

import com.google.android.gcm.GCMRegistrar;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

public class ConfigActivity extends PreferenceActivity implements OnPreferenceChangeListener {
    SharedPreferences pref;
    CheckBoxPreference useGCM;
    EditTextPreference targetlock;
    EditTextPreference myname;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref);
        
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        
        useGCM = (CheckBoxPreference)findPreference("key.useGCM");
        targetlock = (EditTextPreference)findPreference("key.targetlock");
        myname = (EditTextPreference)findPreference("key.myname");
        targetlock.setOnPreferenceChangeListener(this);
        myname.setOnPreferenceChangeListener(this);
        useGCM.setOnPreferenceChangeListener(this);
        
        boolean useGCM = pref.getBoolean("key.useGCM", true);
        
        refreshDisp(null, null);
        
        if(useGCM){
            boolean registerd = pref.getBoolean("key.registerd", false);
            if(!registerd){
                //GCM
                if (GCMRegistrar.isRegistered(this)) {
                    Thread t = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            if(ServerUtilities.register(ConfigActivity.this, GCMRegistrar.getRegistrationId(ConfigActivity.this))){
                                Editor editor = pref.edit();
                                editor.putBoolean("key.registerd", true);
                                editor.commit();
                            }
                        }
                    });
                    t.start();
                }
                else{
                    GCMRegistrar.register(this, CommonUtilities.SENDER_ID);
                }
            }
        }
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        if("key.useGCM".equals(preference.getKey())){
            boolean b = (Boolean)newValue;
            if(b){
                if (GCMRegistrar.isRegistered(this)) {
                    Thread t = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            if(ServerUtilities.register(ConfigActivity.this, GCMRegistrar.getRegistrationId(ConfigActivity.this))){
                                Editor editor = pref.edit();
                                editor.putBoolean("key.registerd", true);
                                editor.commit();
                            }
                        }
                    });
                    t.start();
                }
                else{
                    GCMRegistrar.register(this, CommonUtilities.SENDER_ID);
                }
            }
            else{
                if (GCMRegistrar.isRegistered(this)) {
                    Thread t = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            GCMRegistrar.unregister(ConfigActivity.this);
                            Editor editor = pref.edit();
                            editor.putBoolean("key.registerd", false);
                            editor.commit();
                        }
                    });
                    t.start();
                }
            }
        }
        else{
            refreshDisp(preference, newValue);
        }
        return true;
    }
    
    private void refreshDisp(Preference preference, Object newValue){
        if(preference == null || preference.getKey().equals("key.targetlock")){
            String s = preference == null?pref.getString("key.targetlock", ""):(String)newValue;
            if(Funcs.isNotEmpty(s)){
                targetlock.setSummary(s);
            }
            else{
                myname.setSummary(getString(R.string.lb_targetlock_summary));
            }
        }
        if(preference==null|| preference.getKey().equals("key.myname")){
            String s = preference == null?pref.getString("key.myname", ""):(String)newValue;
            if(Funcs.isNotEmpty(s)){
                myname.setSummary(s);
            }
            else{
                myname.setSummary(getString(R.string.lb_myname_summary));
            }
        }
    }
}
