
package jp.co.kayo.android.localplayer.share.gcm;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;

public class Funcs {

    public static boolean isNotEmpty(String s){
        if(s!=null && s.length()>0){
            return true;
        }
        return false;
    }
    
    public static String makeTimeString(long msecs) {
        if (msecs > 0) {
            long sec = msecs / 1000;
            // _timefmt.flush();
            return String.format("%1$2d:%2$02d",
                    new Object[] { sec / 60, sec % 60 }).toString();
        } else {
            return "0:00";
        }
    }

    public static String getMailAccount(Context context) {
        String id = null;
        Account[] accounts = AccountManager.get(context).getAccounts();
        for (Account account : accounts) {
            String name = account.name;
            if (name.indexOf("@") != -1) {
                id = name;
                break;
            }
            id = name;
        }
        Logger.d("regist mail="+id);

        return id;
    }
    
    private static String bytesToHexString(byte[] fromByte) {

        StringBuilder hexStrBuilder = new StringBuilder();
        for (int i = 0; i < fromByte.length; i++) {

            // 16進数表記で1桁数値だった場合、2桁目を0で埋める
            if ((fromByte[i] & 0xff) < 0x10) {
                hexStrBuilder.append("0");
            }
            hexStrBuilder.append(Integer.toHexString(0xff & fromByte[i]));
        }

        return hexStrBuilder.toString();
    }
    
    public static String encodePassdigiest(String password) {
        byte[] enclyptedHash = null;
        // MD5で暗号化したByte型配列を取得する
        MessageDigest md5;
        try {
            md5 = MessageDigest.getInstance("MD5");
            md5.update(password.getBytes());
            enclyptedHash = md5.digest();

            // 暗号化されたByte型配列を、16進数表記文字列に変換する
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return bytesToHexString(enclyptedHash);
    }
}
