package jp.co.kayo.android.localplayer.share.gcm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class ScrobbleReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent i) {
        if(i!=null && "jp.co.kayo.android.localplayer.appwidget.APPWIDGET_DISPLAY".equals(i.getAction())){
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
            if(pref.getBoolean("key.autoPost", false)){
                Intent intent = new Intent(context, AutoSendService.class);
                intent.putExtra("mediaId", i.getStringExtra("mediaId"));
                intent.putExtra("artist", i.getStringExtra("artist"));
                intent.putExtra("title", i.getStringExtra("title"));
                intent.putExtra("album", i.getStringExtra("album"));
                intent.putExtra("duration", i.getLongExtra("duration", 0));
                intent.putExtra("data", i.getStringExtra("data"));
                intent.putExtra("stat", i.getIntExtra("stat", 0));
                context.startService(intent);
            }
        }
    }
}
