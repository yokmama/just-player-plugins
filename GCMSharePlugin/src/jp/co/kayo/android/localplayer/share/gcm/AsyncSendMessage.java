
package jp.co.kayo.android.localplayer.share.gcm;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class AsyncSendMessage extends AsyncTask<Void, Void, Void> {
    public static final String SERVER_URL = "http://justplayer-dev.appspot.com";
    private Handler mHandler;
    private String mToId;
    private String mName;
    private String mMessage;
    private String mSendData;

    public AsyncSendMessage(Handler handler, String toId, String name, String message, String sendData) {
        mHandler = handler;
        mToId = toId;
        mName = name;
        mMessage = message;
        mSendData = sendData;
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            StringBuilder path = new StringBuilder();
            path.append(SERVER_URL).append("/SendMessage?");
            path.append("toKey=").append(mToId);
            path.append("&name=").append(URLEncoder.encode(mName, "UTF-8"));
            path.append("&message=").append(URLEncoder.encode(mMessage, "UTF-8"));
            path.append("&data=").append(URLEncoder.encode(mSendData, "UTF-8"));
            JSONObject json = new HttpUtils().getJSON(path.toString());
            if(json!=null){
                String status = json.getString("status");
                if("ok".equals(status)){
                    //ok toast
                    mHandler.sendEmptyMessage(1000);
                    return null;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //ng toast
        mHandler.sendEmptyMessage(1001);

        return null;
    }

}
