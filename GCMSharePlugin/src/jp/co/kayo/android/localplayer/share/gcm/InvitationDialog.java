package jp.co.kayo.android.localplayer.share.gcm;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

public class InvitationDialog extends Activity implements OnClickListener {
    String mailtTo  = "";
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_invitation);
        
        findViewById(R.id.button1).setOnClickListener(this);
        findViewById(R.id.button2).setOnClickListener(this);
        
        if(getIntent()!=null && getIntent().getExtras()!=null){
            mailtTo = getIntent().getExtras().getString("key.mailto");
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.button1){
            finish();
        }else if(v.getId() == R.id.button2){
            Uri uri=Uri.parse("mailto:"+mailtTo);
            Intent intent=new Intent(Intent.ACTION_SENDTO, uri);
            intent.putExtra(Intent.EXTRA_SUBJECT,getString(R.string.gcm_invitation_subject));
            intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.gcm_invitation_body));
            try{
                startActivity(intent);
                finish();
            }
            catch(Exception e){
            }
        }
        
    }

}
