package jp.co.kayo.android.localplayer.share.gcm;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */


import android.util.Log;

public class Logger {
    public static final boolean debug = true;
    private static final String TAG = "JUSTPLAYER_GCM";

    public static int d(String msg) {
        if (debug && msg != null) {
            return Log.d(TAG, msg);
        } else
            return 0;
    }

    public static int v(String msg) {
        if (debug && msg != null) {
            return Log.v(TAG, msg);
        }
        return 0;
    }

    public static int i(String msg) {
        if (debug && msg != null) {
            return Log.i(TAG, msg);
        }
        return 0;
    }

    public static int e(String msg, Exception e) {
        if (msg != null) {
            return Log.e(TAG, msg, e);
        }
        return 0;
    }
}
