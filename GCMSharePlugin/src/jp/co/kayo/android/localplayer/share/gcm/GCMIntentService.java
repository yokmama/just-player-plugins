/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.co.kayo.android.localplayer.share.gcm;

import java.net.URLDecoder;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;
import com.google.android.gcm.GCMRegistrar;

/**
 * IntentService responsible for handling GCM messages.
 */
public class GCMIntentService extends GCMBaseIntentService {

    @SuppressWarnings("hiding")
    private static final String TAG = "GCMIntentService";

    public GCMIntentService() {
        super(CommonUtilities.SENDER_ID);
    }

    @Override
    protected void onRegistered(Context context, String registrationId) {
        Log.i(TAG, "Device registered: regId = " + registrationId);
        CommonUtilities.displayMessage(context, getString(R.string.gcm_registered));
        ServerUtilities.register(context, registrationId);
    }

    @Override
    protected void onUnregistered(Context context, String registrationId) {
        Log.i(TAG, "Device unregistered");
        CommonUtilities.displayMessage(context, getString(R.string.gcm_unregistered));
        if (GCMRegistrar.isRegisteredOnServer(context)) {
            ServerUtilities.unregister(context, registrationId);
        } else {
            // This callback results from the call to unregister made on
            // ServerUtilities when the registration to the server failed.
            Log.i(TAG, "Ignoring unregister callback");
        }
    }

    @Override
    protected void onMessage(Context context, Intent intent) {
        Log.i(TAG, "Received message");
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        boolean useGCM = pref.getBoolean("key.useGCM", true);
        if(useGCM){
            // notifies user
            try{
                String name = URLDecoder.decode(intent.getStringExtra("name"), "UTF-8");
                String message = URLDecoder.decode(intent.getStringExtra("message"), "UTF-8");
                String data = URLDecoder.decode(intent.getStringExtra("data"), "UTF-8");
                generateNotification(context, String.format(context.getString(R.string.gcm_request_msg), name), name, message, data);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDeletedMessages(Context context, int total) {
        Log.i(TAG, "Received deleted messages notification");
        String message = getString(R.string.gcm_deleted, total);
        CommonUtilities.displayMessage(context, message);
        // notifies user
        generateNotification(context, context.getString(R.string.gcm_delete_msg), null, message, null);
    }

    @Override
    public void onError(Context context, String errorId) {
        Log.i(TAG, "Received error: " + errorId);
        CommonUtilities.displayMessage(context, getString(R.string.gcm_error, errorId));
    }

    @Override
    protected boolean onRecoverableError(Context context, String errorId) {
        // log message
        Log.i(TAG, "Received recoverable error: " + errorId);
        CommonUtilities.displayMessage(context, getString(R.string.gcm_recoverable_error,
                errorId));
        return super.onRecoverableError(context, errorId);
    }

    /**
     * Issues a notification to inform the user that server has sent a message.
     */
    private static void generateNotification(Context context, String title, String name, String message, String data) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        boolean auto = pref.getBoolean("key.autoRecieve", false);
        if(auto){
            if(data!=null){
                try {
                    JSONObject json = new JSONObject(data);
                    String json_title = json.getString("title");
                    String json_album = json.getString("album");
                    String json_artist = json.getString("artist");
                    long json_duration = json.getLong("duration");
                    String json_url = json.getString("uri");
                    if(Funcs.isNotEmpty(json_url)){
                        String cntkey = SystemConsts.CONTENTSKEY_PLAYLIST
                                + System.currentTimeMillis();
                        
                        Intent broadcast = new Intent("jp.co.kayo.android.localplayer.MEDIA");
                        //broadcast.setAction("jp.co.kayo.android.localplayer.MEDIA");
                        broadcast.putExtra("key.eventType", 1);
                        broadcast.putExtra("key.contentsKey", cntkey);
                        broadcast.putExtra("key.duration", json_duration);
                        broadcast.putExtra("key.title", json_title);
                        broadcast.putExtra("key.album", json_album);
                        broadcast.putExtra("key.artist", json_artist);
                        broadcast.putExtra("key.url", json_url);
                        context.sendBroadcast(broadcast);
                        return;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        
        int icon = R.drawable.ic_stat_name;
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(icon, message, when);
        Intent notificationIntent = new Intent(context, GcmReceiveActivity.class);
        if(name!=null){
            notificationIntent.putExtra("name", name);
        }
        notificationIntent.putExtra("when", when);
        if(message!=null){
            notificationIntent.putExtra("message", message);
        }
        if(data!=null){
            notificationIntent.putExtra("data", data);
        }
        PendingIntent intent =
                PendingIntent.getActivity(context, 0, notificationIntent, Intent.FLAG_ACTIVITY_NEW_TASK);
        notification.setLatestEventInfo(context, title, message, intent);
        notification.flags = Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(0, notification);
    }

}
