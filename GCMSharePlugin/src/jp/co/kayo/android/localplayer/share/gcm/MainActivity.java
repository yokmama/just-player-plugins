
package jp.co.kayo.android.localplayer.share.gcm;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.text.Editable;
import android.text.TextWatcher;

public class MainActivity extends Activity implements OnClickListener, TextWatcher {
    
    SharedPreferences pref;
    TextView textView1;
    TextView textView2;
    AutoCompleteTextView editText1;
    EditText editText2;
    EditText editText3;
    Button button1;
    Button btnContact;
    String sendData;

    String title;
    String artist;
    String album;
    String data;
    long duration;
    
    private Set<String> set_emails = new HashSet<String>();

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == 100) {
                if(data.getData()!=null){
                    Cursor cursor = null;
                    try{
                        cursor = getContentResolver().query(data.getData(), new String[]{ContactsContract.CommonDataKinds.Email.ADDRESS}, null, null, null);
                        if(cursor!=null && cursor.moveToFirst()){
                            String email = cursor.getString(0);
                            this.editText1.setText(email);
                        }
                    }
                    finally{
                        if(cursor!=null){
                            cursor.close();
                        }
                    }
                }
            }

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_gcm);

        pref = PreferenceManager.getDefaultSharedPreferences(this);

        textView1 = (TextView) findViewById(R.id.textView1);
        textView2 = (TextView) findViewById(R.id.textView2);
        editText1 = (AutoCompleteTextView) findViewById(R.id.editText1);
        editText2 = (EditText) findViewById(R.id.editText2);
        editText3 = (EditText) findViewById(R.id.editText3);
        button1 = (Button) findViewById(R.id.button1);
        btnContact = (Button) findViewById(R.id.btnContact);
        editText3.addTextChangedListener(this);
        button1.setOnClickListener(this);
        btnContact.setOnClickListener(this);
        findViewById(R.id.button2).setOnClickListener(this);
        
        Intent it = getIntent();
        if (it != null) {
            album = it.getStringExtra("album");
            artist = it.getStringExtra("artist");
            title = it.getStringExtra("title");
            data = it.getStringExtra("data");
            duration = it.getLongExtra("duration", 0);
        }
        
        
        if (Funcs.isNotEmpty(title) && Funcs.isNotEmpty(artist)) {
            textView1.setText(title);
            
            editText1.setAdapter(createAdapter());
            
            String target = pref.getString("key.targetlock", "");
            String myName = pref.getString("key.myname", "");
            if(Funcs.isNotEmpty(target)){
                editText1.setText(target);
            }
            if(!Funcs.isNotEmpty(myName)){
                myName = Funcs.getMailAccount(this);
            }
            
            editText2.setText(myName);
            editText3.setText(title + " - " + artist);
            try {
                JSONObject json = new JSONObject();
                json.put("title", title);
                json.put("album", album);
                json.put("artist", artist);
                json.put("duration", duration);
                if (data.startsWith("http")) {
                    json.put("uri", data);
                }
                else {
                    json.put("uri", "");
                }

                sendData = json.toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        
        }else{
            finish();
        }
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        saveAdapter();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_settings) {
            Intent intent = new Intent(this, ConfigActivity.class);
            startActivityForResult(intent, 10);
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button1) {
            String email = editText1.getText().toString();
            if (email != null && email.length() > 0) {
                set_emails.add(email);
                String toKey = Funcs.encodePassdigiest(email);
                String name = editText2.getText().toString();
                String message = editText3.getText().toString();
                try {
                    if (message.indexOf(YOUTUBE_LINK) != -1) {
                        StringBuilder url = new StringBuilder();
                        url.append("http://www.youtube.com/results?search_query=")
                                .append(URLEncoder.encode(artist, "UTF-8")).append("+")
                                .append(URLEncoder.encode(title, "UTF-8"));

                        String youtubelinke = url.toString();
                        message = message.replace(YOUTUBE_LINK, youtubelinke);
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    message = message.replace(YOUTUBE_LINK, "");
                }

                AsyncSendMessage task = new AsyncSendMessage(new MyHandler(this, email),
                        toKey, name, message, sendData);
                task.execute();

                // end
                finish();
            }
            else {
                Toast.makeText(this, getString(R.string.gcm_dest_input_error), Toast.LENGTH_SHORT)
                        .show();
            }
        } else if (v.getId() == R.id.button2) {
            addYoutubeLink();
        } else if(v.getId() == R.id.btnContact){
            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Email.CONTENT_URI);
            startActivityForResult(intent, 100);
        }
    }

    private String YOUTUBE_LINK = "http://t.co/XXXXXXXX";

    private void addYoutubeLink() {
        StringBuilder buf = new StringBuilder();
        String text = editText3.getText().toString();
        buf.append(text);
        buf.append(" ");

        buf.append(YOUTUBE_LINK);

        editText3.setText(buf.toString());
    }

    static class MyHandler extends Handler {
        Context context;
        String email;

        public MyHandler(Context c, String email) {
            this.context = c;
            this.email = email;
        }

        @Override
        public void handleMessage(Message msg) {

            if (msg.what == 1000) {
                Toast.makeText(context, context.getString(R.string.gcm_request_ok),
                        Toast.LENGTH_SHORT).show();
            }
            else {
                /*
                 * Toast.makeText(context,
                 * context.getString(R.string.gcm_request_ng),
                 * Toast.LENGTH_SHORT).show();
                 */
                Intent i = new Intent(context, InvitationDialog.class);
                i.putExtra("key.mailto", email);
                context.startActivity(i);
            }
        }

    }

    @Override
    public void afterTextChanged(Editable s) {
        String str = s.toString();
        final int n = 140 - str.length();
        textView2.setText(Integer.toString(n));
        if (n >= 0) {
            button1.setEnabled(true);
        } else {
            button1.setEnabled(false);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }
    
    void saveAdapter(){
        StringBuilder buf = new StringBuilder();
        for(Iterator<String> ite=set_emails.iterator(); ite.hasNext(); ){
            if(buf.length()>0){
                buf.append(",");
            }
            buf.append(ite.next());
        }
        Editor editor = pref.edit();
        editor.putString("key.history", buf.toString());
        editor.commit();
    }
    
    ArrayAdapter<String> createAdapter(){
        set_emails.clear();
        String str_emails = pref.getString("key.history", "");
        String[] emails = str_emails.split(",");
        for(String email : emails){
            set_emails.add(email);
        }
        
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, set_emails.toArray(new String[set_emails.size()]));
        return adapter;
    }
 }
