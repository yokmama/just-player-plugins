
package jp.co.kayo.android.localplayer.share.gcm;

import java.util.Calendar;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class GcmReceiveActivity extends Activity implements OnClickListener {
    
    TextView textDate;
    TextView textFrom;
    TextView textTitle;
    TextView textArtist;
    TextView textAlbum;
    TextView textDuration;
    TextView textMessage;
    Button button1;
    long duration;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_gcmrec);
        textDate = (TextView) findViewById(R.id.textDate);
        textFrom = (TextView) findViewById(R.id.textFrom);
        textTitle = (TextView) findViewById(R.id.textTitle);
        textArtist = (TextView) findViewById(R.id.textArtist);
        textAlbum = (TextView) findViewById(R.id.textAlbum);
        textDuration = (TextView) findViewById(R.id.textDuration);
        textMessage = (TextView) findViewById(R.id.textMessage);
        button1 = (Button) findViewById(R.id.button1);

        button1.setOnClickListener(this);

        Intent intent = getIntent();
        if (intent != null) {
            try {
                String name = intent.getStringExtra("name");
                if (name != null) {
                    textFrom.setText(name);
                }
                long when = intent.getLongExtra("when", 0);
                if (when != 0) {
                    Calendar cal = Calendar.getInstance();
                    cal.setTimeInMillis(when);
                    textDate.setText(DateFormat.getMediumDateFormat(this).format(cal.getTime()));
                }
                String message = intent.getStringExtra("message");
                if (message != null) {
                    textMessage.setText(message);
                }

                String data = intent.getStringExtra("data");
                if (data != null) {
                    JSONObject json = new JSONObject(data);
                    textTitle.setText(json.getString("title"));
                    textAlbum.setText(json.getString("album"));
                    textArtist.setText(json.getString("artist"));
                    duration = json.getLong("duration");
                    url = json.getString("uri");
                    if (duration > 0) {
                        textDuration.setText(Funcs.makeTimeString(duration));
                    }
                    if (url == null || !url.startsWith("http")) {
                        button1.setText(getString(R.string.alert_dialog_close));
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button1) {
            if (url != null && url.startsWith("http")) {
                String cntkey = SystemConsts.CONTENTSKEY_PLAYLIST
                        + System.currentTimeMillis();
                
                Intent broadcast = new Intent("jp.co.kayo.android.localplayer.MEDIA");
                //broadcast.setAction("jp.co.kayo.android.localplayer.MEDIA");
                broadcast.putExtra("key.eventType", 1);
                broadcast.putExtra("key.contentsKey", cntkey);
                broadcast.putExtra("key.duration", duration);
                broadcast.putExtra("key.title", textTitle.getText().toString());
                broadcast.putExtra("key.album", textAlbum.getText().toString());
                broadcast.putExtra("key.artist", textArtist.getText().toString());
                broadcast.putExtra("key.url", url);
                sendBroadcast(broadcast);
            }
            finish();
        }
    }
}
