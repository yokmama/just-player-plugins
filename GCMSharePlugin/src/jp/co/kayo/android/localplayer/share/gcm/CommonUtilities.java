/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.co.kayo.android.localplayer.share.gcm;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import jp.co.kayo.android.localplayer.secret.Keys;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;

/**
 * Helper class providing methods and constants common to other classes in the
 * app.
 */
public final class CommonUtilities {

    /**
     * Base URL of the Demo Server (such as http://my_host:8080/gcm-demo)
     */
    public static final String SERVER_URL = Keys.SERVER_URL;

    /**
     * Google API project id registered to use GCM.
     */
    static final String SENDER_ID = Keys.SENDER_ID;

    /**
     * Intent used to display a message in the screen.
     */
    static final String DISPLAY_MESSAGE_ACTION =
            "jp.co.kayo.android.localplayer.DISPLAY_MESSAGE";

    /**
     * Intent's extra that contains the message to be displayed.
     */
    static final String EXTRA_MESSAGE = "message";

    /**
     * Notifies UI to display a message.
     * <p>
     * This method is defined in the common helper because it's used both by
     * the UI and the background service.
     *
     * @param context application's context.
     * @param message message to be displayed.
     */
    static void displayMessage(Context context, String message) {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        context.sendBroadcast(intent);
    }
    
    private static String bytesToHexString(byte[] fromByte) {

        StringBuilder hexStrBuilder = new StringBuilder();
        for (int i = 0; i < fromByte.length; i++) {

            // 16進数表記で1桁数値だった場合、2桁目を0で埋める
            if ((fromByte[i] & 0xff) < 0x10) {
                hexStrBuilder.append("0");
            }
            hexStrBuilder.append(Integer.toHexString(0xff & fromByte[i]));
        }

        return hexStrBuilder.toString();
    }
    
    public static String encodePassdigiest(String password) {
        byte[] enclyptedHash = null;
        // MD5で暗号化したByte型配列を取得する
        MessageDigest md5;
        try {
            md5 = MessageDigest.getInstance("MD5");
            md5.update(password.getBytes());
            enclyptedHash = md5.digest();

            // 暗号化されたByte型配列を、16進数表記文字列に変換する
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return bytesToHexString(enclyptedHash);
    }

    public static String getMailAccount(Context context) {
        String id = null;
        Account[] accounts = AccountManager.get(context).getAccounts();
        for (Account account : accounts) {
            String name = account.name;
            if (name.indexOf("@") != -1) {
                id = name;
                break;
            }
            id = name;
        }
        Logger.d("regist mail="+id);

        return id;
    }
    
    /*
    public static String getDeviceKey(Context context) {
        return encodePassdigiest(getMailAccount(context));
    }*/
    
    public static String getDeviceKeys(Context context) {
        StringBuilder buf = new StringBuilder();
        
        Account[] accounts = AccountManager.get(context).getAccounts();
        for (Account account : accounts) {
            String name = account.name;
            if (name.indexOf("@") != -1) {
                if(buf.length()>0){
                    buf.append(",");
                }
               
                buf.append(encodePassdigiest(name));
            }
        }

        return buf.toString();
    }
}
