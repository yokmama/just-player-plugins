package jp.co.kayo.android.localplayer.share.gcm;

public interface SystemConsts {
    public static final String CONTENTSKEY_ALBUM = "Album.";
    public static final String CONTENTSKEY_ARTIST = "Artist.";
    public static final String CONTENTSKEY_ARTIST_TITLE = "ArtistTitle.";
    public static final String CONTENTSKEY_PLAYLIST = "Playlist.";
    public static final String CONTENTSKEY_MEDIA = "Media.";
    public static final String CONTENTSKEY_GENRES = "Genres.";
    public static final String CONTENTSKEY_PLAYALBUM = "PlayAlbum.";
    public static final String CONTENTSKEY_SEARCH = "Search.";
    public static final String CONTENTSKEY_FOLDER = "Folder.";

}
