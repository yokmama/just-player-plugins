
package jp.co.kayo.android.localplayer.share.gcm;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import jp.co.kayo.android.localplayer.secret.Keys;
import jp.co.kayo.android.localplayer.share.gcm.MainActivity.MyHandler;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;

public class AutoSendService extends Service {
    // 前回のツイートから５秒以上たってないとつぶやけない
    public static final long TWEET_DELAY = 1000 * 5;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
            String target = pref.getString("key.targetlock", "");
            String myName = pref.getString("key.myname", "");
            if (Funcs.isNotEmpty(target)) {
                String artist = intent.getStringExtra("artist");
                String title = intent.getStringExtra("title");
                String data = intent.getStringExtra("data");
                //クラウドデータ以外は自動で送信しない
                if(Funcs.isNotEmpty(data)){
                    if(!Funcs.isNotEmpty(myName)){
                        myName = Funcs.getMailAccount(this);
                    }
                    
                    long lastweetTime = pref.getLong("lastGcm.time", 0);
                    String lasttweetText = pref.getString("lastGcm.text", "");
                    long current = System.currentTimeMillis();
                    if (!lasttweetText.equals(title + artist)
                            && (current - lastweetTime) > TWEET_DELAY) {
                        
                        String toKey = Funcs.encodePassdigiest(target);
                        String name = myName;
                        String message = title + " - " + artist;
                        String album = intent.getStringExtra("album");
                        long duration = intent.getLongExtra("duration", 0);
                        String sendData = null;
                        try {
                            JSONObject json = new JSONObject();
                            json.put("title", title);
                            json.put("album", album);
                            json.put("artist", artist);
                            json.put("duration", duration);
                            if (data.startsWith("http")) {
                                json.put("uri", data);
                            }
                            else {
                                json.put("uri", "");
                            }
    
                            sendData = json.toString();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        
                        
                        AsyncSendMessage task = new AsyncSendMessage(new Handler(),
                                toKey, name, message, sendData);
                        task.execute();
                    }
                }
            }
        }

        stopSelf();

        return START_NOT_STICKY;
    }

}
