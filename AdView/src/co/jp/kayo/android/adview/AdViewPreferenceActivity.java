package co.jp.kayo.android.adview;

import java.util.Calendar;
import java.util.Date;

import co.jp.kayo.android.adview.ad.AdHelper;
import co.jp.kayo.android.adview.ad.AdView;
import android.preference.PreferenceActivity;

public class AdViewPreferenceActivity extends PreferenceActivity {
    private Date srcDate = Calendar.getInstance().getTime();
    private AdView mAdView;
    
    private AdView getAdView(){
        if(mAdView == null){
            mAdView = (AdView) findViewById(R.id.AdView);
        }
        return mAdView;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(getAdView()!=null){
            // 起動から1日以上経過していれば広告を更新
            int elapsed = AdHelper.differenceDays(srcDate, new Date());
            if (elapsed >= 1) {
                getAdView().refreshAdList();
                srcDate = Calendar.getInstance().getTime();
            }
        }
    }

}
