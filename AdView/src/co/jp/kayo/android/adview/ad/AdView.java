
package co.jp.kayo.android.adview.ad;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import co.jp.kayo.android.adview.R;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class AdView extends ImageView implements OnClickListener {
    private static final String JSON_FILE = "ad.json";
    private static final String BASE_URL = "http://www13080ue.sakura.ne.jp/kayo";
    private static final int REFRESH_INTERVALE = 10 * 1000; // リフレッシュインターバル(s)
                                                            // ※デフォルト10秒
    private static final String DEFAULT_BANNER_LINK = "http://kayosystem.blogspot.jp/"; // 　デフォルトバナーのリンク
    private static int adIndex = 0;
    private final Context context;
    private static List<AdsData> adsList;
    public static Handler mHandler = new Handler();

    public AdView(final Context context) {
        super(context);
        this.context = context;
    }

    public AdView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        if (adsList == null) {
            adsList = new ArrayList<AdsData>();
            initAdList();
            LoadAdTask task = new LoadAdTask();
            task.execute();
        } else {
            adIndex++;
            if (adsList.size() <= adIndex) {
                adIndex = 0;
            }
            if (adsList.size() != 0) {
                setImageDrawable(adsList.get(adIndex).getImgDrawable());
            } else {
                initAdList();
            }
        }
        setOnClickListener(this);
    }

    /**
     * 広告リストの初期化（デフォルトの公告もここで作成）
     */
    public void initAdList() {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inScaled = true;
        opt.inDensity = AdsData.DENSITY;
        opt.inScreenDensity = DisplayMetrics.DENSITY_HIGH;

        AdsData data = new AdsData();
        data.setImgBitmap(BitmapFactory.decodeResource(getResources(),
                R.drawable.adview_banner, opt));
        data.setLink(DEFAULT_BANNER_LINK);
        adsList.add(data);
        setImageDrawable(data.getImgDrawable());
    }

    /**
     * 広告の更新処理
     */
    public void refreshAdList() {
        adsList.clear();
        mHandler.removeCallbacks(mAdRefresh);
        initAdList();
        LoadAdTask task = new LoadAdTask();
        task.execute();
    }

    /**
     * 広告の読み込み処理
     */
    public class LoadAdTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            // 国別のJSONファイルを選択
            String jsonUrl = "";
            Locale locale = Locale.getDefault();
            if (locale.toString().equals("ja_JP")) {
                jsonUrl = BASE_URL + "/ja/" + JSON_FILE;
            } else {
                jsonUrl = BASE_URL + "/en/" + JSON_FILE;
            }

            String jsonStr = "";
            try {
                URL url = new URL(jsonUrl);
                InputStream is = url.openStream();
                InputStreamReader isr = new InputStreamReader(is, "UTF-8");
                BufferedReader br = new BufferedReader(isr);

                String str = "";
                while ((str = br.readLine()) != null) {
                    jsonStr += str;
                }
                if (jsonStr.indexOf('[') == -1 || jsonStr.indexOf(']') == -1) {
                    return null;
                }

                jsonStr = jsonStr.substring(jsonStr.indexOf('['),
                        jsonStr.lastIndexOf(']') + 1);
                final JSONArray jsons = new JSONArray(jsonStr);
                for (int i = 0; i < jsons.length(); i++) {
                    JSONObject jsonObj = jsons.getJSONObject(i);

                    boolean visible = jsonObj.getBoolean("VISIBLE");
                    if (visible) {
                        final AdsData data = new AdsData();
                        data.setName(jsonObj.getString("NAME"));
                        data.setImg(jsonObj.getString("IMG"));
                        data.setLink(jsonObj.getString("LINK"));
                        adsList.add(data);
                    }
                }
            } catch (final IOException e) {
                e.printStackTrace();
            } catch (final JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected final void onPostExecute(Void result) {
            mHandler.post(mAdRefresh);
        }
    }

    /**
     * リフレッシュインターバル毎に公告を切り替える
     */
    Runnable mAdRefresh = new Runnable() {
        public void run() {
            adIndex++;
            if (adsList.size() == 1) {
                // サイズが1＝ネットワークから画像を全く取得できていない状態
                adIndex = 0;
            } else if (adIndex >= adsList.size()) {
                adIndex = 1;
            }
            setImageDrawable(adsList.get(adIndex).getImgDrawable());
            invalidate();
            mHandler.postDelayed(this, REFRESH_INTERVALE);
        }
    };

    @Override
    public final void onClick(final View arg0) {
        final Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (adsList.size() != 0) {
            intent.setData(Uri.parse(adsList.get(adIndex).getLink()));
        } else {
            intent.setData(Uri.parse(DEFAULT_BANNER_LINK));
        }
        context.startActivity(intent);
    }

}
