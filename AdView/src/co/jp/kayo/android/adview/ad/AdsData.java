
package co.jp.kayo.android.adview.ad;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.NinePatch;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.util.DisplayMetrics;
import android.util.Log;

public class AdsData {
    private String name;
    private String img;
    private Drawable imgDrawable;
    private String link;
    public static final int DENSITY = 240;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(final String img) {
        this.img = img;

        final BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inScaled = true;
        opt.inDensity = DENSITY;
        opt.inScreenDensity = DisplayMetrics.DENSITY_HIGH;
        final Rect rect = new Rect(0, 0, 0, 0);

        try {
            if (getImgDrawable() == null) {
                final URL url = new URL(getImg());
                InputStream is;

                is = url.openStream();

                Bitmap bm = BitmapFactory.decodeStream(is, rect, opt);
                if (bm != null) {
                    if (bm.getNinePatchChunk() != null) {

                        final NinePatch ninePatch = new NinePatch(bm, bm.getNinePatchChunk(), null);
                        imgDrawable = new NinePatchDrawable(ninePatch);
                    } else {

                        imgDrawable = new BitmapDrawable(bm);

                    }
                }
            }

        } catch (final IOException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        }
    }

    /** 広告画像を設定(Bitmapデータ用). */
    public void setImgBitmap(final Bitmap imgBitmap) {

        if (imgBitmap.getNinePatchChunk() != null) {
            final NinePatch ninePatch = new NinePatch(imgBitmap, imgBitmap.getNinePatchChunk(),
                    null);
            imgDrawable = new NinePatchDrawable(ninePatch);
        } else {
            imgDrawable = new BitmapDrawable(imgBitmap);
        }
        Log.d("","sise:"+imgBitmap.getWidth());

    }

    /** 広告画像データを返却. */
    public Drawable getImgDrawable() {
        return imgDrawable;
    }

    /** 広告リンクを返却. */
    public String getLink() {
        return link;
    }

    /** 広告リンクを設定. */
    public void setLink(final String link) {
        this.link = link;
    }
}
