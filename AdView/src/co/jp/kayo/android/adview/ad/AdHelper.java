package co.jp.kayo.android.adview.ad;

import java.util.Date;

public class AdHelper {
    /**
     * 何日経過したか返す
     * 
     * @param date1:日付1
     * @param date2:日付2
     * @return 日数を返却
     */
    public static int differenceDays(Date date1, Date date2) {
        long datetime1 = date1.getTime();
        long datetime2 = date2.getTime();
        long one_date_time = 1000 * 60 * 60 * 24;
        long diffDays = (datetime1 - datetime2) / one_date_time;
        return (int) diffDays;
    }    

}
